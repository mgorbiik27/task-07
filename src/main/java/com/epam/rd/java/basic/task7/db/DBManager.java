package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;


public class DBManager {

    private static final String TABLE_USERS = "users";
    private static final String TABLE_TEAMS = "teams";
    private static final String TABLE_USERS_TEAMS = "users_teams";
    private static final String APP_PROPS_FILE = "app.properties";
    private static final String CONNECTION_URL;
    private static DBManager instance;

    static {
        try {
            String url = Files.readString(Path.of(APP_PROPS_FILE));
            url=url.substring(url.indexOf("=")+1);
            CONNECTION_URL = url;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private DBManager() {
    }

  /*  public static void main(String[] args) {
        try {
            System.out.println("Users: " + new DBManager().findAllUsers());
            var db = getInstance();
            db.clearDB();
            System.out.println("clear");
            System.out.println("Users: " + new DBManager().findAllUsers());
            System.out.println("Teams: " + new DBManager().findAllTeams());

            User user = new User(100, "some name");
            System.out.println("InsertU:" + db.insertUser(user));
            System.out.println("InsertT:" + db.insertTeam(Team.createTeam("Some team1")));
            System.out.println("InsertT:" + db.insertTeam(Team.createTeam("Some team2")));
            System.out.println("InsertT:" + db.insertTeam(Team.createTeam("Some team3")));
            System.out.println("InsertT:" + db.insertTeam(Team.createTeam("Some team4")));
            Team team = Team.createTeam("Team1");
            Team team2 = Team.createTeam("Team2");
            System.out.println("InsertT:" + db.insertTeam(team));
            System.out.println("InsertT:" + db.insertTeam(team2));

            System.out.println("Users: " + new DBManager().findAllUsers());
            System.out.println("Teams: " + new DBManager().findAllTeams());
            System.out.println("GetUser:" + db.getUser("some name"));
            System.out.println("Users: " + new DBManager().findAllUsers());
            try {
                System.out.println(db.setTeamsForUser(user, team, team));
            } catch (DBException e) {
                System.out.println("Insertion failed for duplicate teams");
            }
            System.out.println("User teams: " + db.getUserTeams(user));
            System.out.println(db.setTeamsForUser(user, team, team2));
            System.out.println("User teams: " + db.getUserTeams(user));

        } catch (DBException e) {
            System.out.println(e.getCause().getMessage());
            e.printStackTrace();
        }
    }*/

    public static synchronized DBManager getInstance() {
        if (instance == null) instance = new DBManager();
        return instance;
    }

    public void clearDB() throws DBException {
        Connection con = null;
        Statement statement = null;
        try {
            con = getConnection();
            statement = con.createStatement();
            con.setAutoCommit(false);
            statement.executeUpdate("truncate table users_teams cascade");
            statement.executeUpdate("truncate table users cascade");
            statement.executeUpdate("truncate table teams cascade");
            con.commit();

        } catch (SQLException e) {
            try {
                if (con != null) con.rollback();
            } catch (SQLException ex) {
                throw new DBException("Failed to rollback db truncation", ex);
            }
            throw new DBException("Unable to clear database", e);
        } finally {
            try {
                if (con != null) con.close();
                if (statement != null) statement.close();
            } catch (SQLException e) {
                throw new DBException("Unable to close resources", e);
            }

        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(CONNECTION_URL);
    }

    public List<User> findAllUsers() throws DBException {
        String statement = String.format("select * from %s", TABLE_USERS);
        try (Connection con = getConnection(); PreparedStatement preparedStatement = con.prepareStatement(statement)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            List<User> users = new ArrayList<>();
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            throw new DBException("Users were not found due to an error", e);
        }
    }

    public int getUsersCount() throws DBException {
        String statement = String.format("select max(id) from %s", TABLE_USERS);
        try (Connection con = getConnection(); PreparedStatement preparedStatement = con.prepareStatement(statement)) {
            var res = preparedStatement.executeQuery();
            res.next();
            return res.getInt(1);
        } catch (SQLException e) {
            throw new DBException("Unable to get users count", e);
        }
    }

    public int getTeamsCount() throws DBException {
        String statement = String.format("select max(id) from %s", TABLE_TEAMS);
        try (Connection con = getConnection(); PreparedStatement preparedStatement = con.prepareStatement(statement)) {
            var res = preparedStatement.executeQuery();
            res.next();
            return res.getInt(1);
        } catch (SQLException e) {
            throw new DBException("Unable to get teams count", e);
        }
    }

    public boolean insertUser(User user) throws DBException {
        String statement = String.format("insert into %s (%s) values(?)", TABLE_USERS, User.class.getDeclaredFields()[1].getName());
        try (Connection con = getConnection(); PreparedStatement preparedStatement = con.prepareStatement(statement)) {
            preparedStatement.setString(1, user.getLogin());
            boolean result = preparedStatement.executeUpdate() == 1;

            PreparedStatement idStatement = con.prepareStatement(String.format("select id from %s where login=?", TABLE_USERS));
            idStatement.setString(1, user.getLogin());
            ResultSet idResult = idStatement.executeQuery();
            idResult.next();
            user.setId(idResult.getInt(1));
            return result;
        } catch (SQLException e) {
            throw new DBException("Insertion was not successful; properties: " + CONNECTION_URL + ";\n" + e.getMessage(), e);
        }
    }

    public boolean deleteUsers(User... users) throws DBException {

        PreparedStatement preparedStatement = null;
        try (Connection con = getConnection()) {
            StringJoiner sj = new StringJoiner(",");
            for (User user : users) {
                sj.add(String.valueOf(user.getId()));
            }
            String statement = String.format("delete from %s where id in (%s)", TABLE_USERS, sj);
            preparedStatement = con.prepareStatement(statement);

            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DBException("Deletion was unsuccessful due to an error", e);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public User getUser(String login) throws DBException {
        String statement = String.format("select * from %s where %s = ?", TABLE_USERS, "login");
        try (Connection con = getConnection(); PreparedStatement preparedStatement = con.prepareStatement(statement)) {
            preparedStatement.setString(1, login);

            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            User user = new User();
            user.setId(resultSet.getInt("id"));
            user.setLogin(resultSet.getString("login"));
            return user;
        } catch (SQLException e) {
            throw new DBException("Unable to get user due to an error", e);
        }
    }

    public Team getTeam(String name) throws DBException {
        String statement = String.format("select * from %s where %s = ?", TABLE_TEAMS, "name");
        try (Connection con = getConnection(); PreparedStatement preparedStatement = con.prepareStatement(statement)) {
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            Team team = Team.createTeam(name);
            team.setId(resultSet.getInt("id"));
            return team;
        } catch (SQLException e) {
            throw new DBException("Unable to get team due to an error", e);
        }
    }

    public List<Team> findAllTeams() throws DBException {
        String statement = String.format("select * from %s", TABLE_TEAMS);
        try (Connection con = getConnection(); PreparedStatement preparedStatement = con.prepareStatement(statement)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Team> teams = new ArrayList<>();
            while (resultSet.next()) {
                Team team = new Team();
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
                teams.add(team);
            }
            return teams;
        } catch (SQLException e) {
            throw new DBException("Teams were not found due to an error", e);
        }
    }

    public boolean insertTeam(Team team) throws DBException {
        String statement = String.format("insert into %s (%s) values(?)", TABLE_TEAMS, Team.class.getDeclaredFields()[1].getName());
        try (Connection con = getConnection(); PreparedStatement preparedStatement = con.prepareStatement(statement)) {
            preparedStatement.setString(1, team.getName());
            boolean result = preparedStatement.executeUpdate() == 1;

            PreparedStatement idStatement = con.prepareStatement(String.format("select id from %s where name=?", TABLE_TEAMS));
            idStatement.setString(1, team.getName());
            ResultSet idResult = idStatement.executeQuery();
            idResult.next();
            team.setId(idResult.getInt(1));
            return result;
        } catch (SQLException e) {
            throw new DBException("Insertion was not successful", e);
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        StringBuilder sb = new StringBuilder();
        for (Team team : teams) {
            sb.append("(").append(user.getId()).append(",").append(team.getId()).append("),");
        }
        if (sb.length() > 1) sb.deleteCharAt(sb.length() - 1);
        String statement = String.format("insert into %s (user_id, team_id) values %s", TABLE_USERS_TEAMS, sb);
        System.out.println("Statement: " + statement);
        try (Connection con = getConnection(); PreparedStatement preparedStatement = con.prepareStatement(statement)) {
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DBException("Insertion was not successful", e);
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        String statement = String.format("select * from %s t where t.id in (select ut.team_id from %s ut where ut.user_id = ?)", TABLE_TEAMS, TABLE_USERS_TEAMS);
        try (Connection con = getConnection(); PreparedStatement preparedStatement = con.prepareStatement(statement)) {
            preparedStatement.setInt(1, user.getId());

            ResultSet resultSet = preparedStatement.executeQuery();
            List<Team> teams = new ArrayList<>();
            while (resultSet.next()) {
                Team t = Team.createTeam(resultSet.getString("name"));
                t.setId(resultSet.getInt("id"));
                teams.add(t);
            }
            return teams;
        } catch (SQLException e) {
            throw new DBException("Unable to get user teams due to an error", e);
        }
    }

    public boolean deleteTeam(Team team) throws DBException {
        String statement = String.format("delete from %s where id = ?", TABLE_TEAMS);
        try (Connection con = getConnection(); PreparedStatement preparedStatement = con.prepareStatement(statement)) {
            preparedStatement.setInt(1, team.getId());
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DBException("Deletion was not successful", e);
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        String statement = String.format("update %s set name = ? where id = ?", TABLE_TEAMS);
        try (Connection con = getConnection(); PreparedStatement preparedStatement = con.prepareStatement(statement)) {
            preparedStatement.setString(1, team.getName());
            preparedStatement.setInt(2, team.getId());
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DBException("Updating was not successful", e);
        }
    }

}
