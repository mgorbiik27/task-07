package com.epam.rd.java.basic.task7.db.entity;

public class User {

    private int id;

    private String login;

    public User() {
    }

    public User(int id, String login) {
        this.id = id;
        this.login = login;
    }

    public static User createUser(String login) {
        User u = new User();
        u.id = 0;
        u.login = login;
        return u;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return login;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return getLogin() != null ? getLogin().equals(user.getLogin()) : user.getLogin() == null;
    }

    @Override
    public int hashCode() {
        return getLogin() != null ? getLogin().hashCode() : 0;
    }
}
